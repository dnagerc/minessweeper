import java.util.Scanner;

public class BoardOfMinesweeper {
    public Box[][] boardOfBoxes;
    private final int ROW_NUMBER_OF_BOARD;
    private final int COLUMN_NUMBER_OF_BOARD;
    private final int MINE_NUMBER_OF_BOARD;

    // Constructor of Minesweeper
    public BoardOfMinesweeper(int numberOfRows, int numberOfColumns, int numberOfMines) {
        this.ROW_NUMBER_OF_BOARD = numberOfRows;
        this.COLUMN_NUMBER_OF_BOARD = numberOfColumns;
        this.MINE_NUMBER_OF_BOARD = numberOfMines;
        initBoardOfBoxes();
    }

    // Init the Boxes of Minesweeper
    public void initBoardOfBoxes() {
        boardOfBoxes = new Box[ROW_NUMBER_OF_BOARD][COLUMN_NUMBER_OF_BOARD];
        for (int row = 0; row < this.ROW_NUMBER_OF_BOARD; row++) {
            for (int column = 0; column < this.COLUMN_NUMBER_OF_BOARD; column++) {
                this.boardOfBoxes[row][column] = new Box(row, column);
            }
        }
        initMinesOfBoard();
    }

    // Init the Mines of Minesweeper
    private void initMinesOfBoard() {
        int numberOfGeneratedMines=0;
        int indexOfMine = 0;
        while (numberOfGeneratedMines != this.MINE_NUMBER_OF_BOARD) {
            int row = (int) (Math.random() * this.ROW_NUMBER_OF_BOARD);
            int column = (int) (Math.random() * this.COLUMN_NUMBER_OF_BOARD);
            if (!this.boardOfBoxes[row][column].isMine()) {
                this.boardOfBoxes[row][column].setMine();
                numberOfGeneratedMines++;
            }
        }
        initNumbersOfMinesweeper();
    }

    // Init the Numbers of Minesweeper
    private void initNumbersOfMinesweeper() {
        for (int row = 0; row < this.ROW_NUMBER_OF_BOARD; row++) {
            for (int column = 0; column < this.COLUMN_NUMBER_OF_BOARD; column++) {

                Box box = this.boardOfBoxes[row][column];

                // Fill With Number
                if(!box.isMine()){
                    // CHECK & SET Mine number to Box
                    MineChecker mineChecker = new MineChecker(box, this.boardOfBoxes);
                    mineChecker.checkMines();
                }
            }
        }
    }

    // Start Program
    public static void main(String[] args) {
        BoardOfMinesweeper boardOfMineSweeper = new BoardOfMinesweeper(6,6,7);
        PrintBoard p = new PrintBoard(boardOfMineSweeper.boardOfBoxes);

        System.out.println("Solved");
        p.printSolvedBoard();

        System.out.println("Unsolved");
        p.printUnsolvedBoard();

        System.out.println("Empty Solved Board");
        p.printEmptySolvedBoard();

        // TODO: Interaction with User (beta version with some problems)
        Scanner s = new Scanner(System.in);
        System.out.print("Introduce (A - F) column and (0 - 5) row to start the game. Example: A0 ");
        String coordinate = s.nextLine();

        int counter = 0;
        char[] letters = {'A', 'B', 'C', 'D', 'E', 'F'};
        for (char letter : letters) {
            if (letter == coordinate.charAt(0)) break;
            counter++;
        }
        System.out.println(boardOfMineSweeper.boardOfBoxes[Integer.parseInt(coordinate.charAt(1)+"")][counter].isMine() ? "You hit a mine."
                : boardOfMineSweeper.boardOfBoxes[Integer.parseInt(coordinate.charAt(1)+"")][counter].getValueOfBox());
    }
}