class PrintBoard {
    private final Box[][] BOXES;


    public PrintBoard(Box[][] boxes) {
        this.BOXES = boxes;
    }

    // Print Solved Board
    public void printSolvedBoard() {
        for (Box[] boxRow : BOXES) {
            for (Box box : boxRow) {
                System.out.print(box.isMine() ? "*" : box.getValueOfBox());
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    // Print Unsolved Board
    public void printUnsolvedBoard() {
        int counter = 0;
        System.out.println("  A B C D E F");
        for (Box[] boxRow : BOXES) {
            System.out.print(counter++ + " ");
            for (Box box : boxRow) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }

    // Print Board With 0/Empty/No Mine Solved
    public void printEmptySolvedBoard() {
        for (Box[] boxRow : BOXES) {
            for (Box box : boxRow) {
                System.out.print(!box.isMine() && box.getValueOfBox() == 0 ? "0" : "*");
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}