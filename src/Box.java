import java.awt.Point;

public class Box {
    private final Point COORDINATE;

    private boolean mine;
    private boolean flag;
    private byte valueOfBox;

    // Constructor
    public Box(int newRow, int newColumn) {
        this.COORDINATE = new Point(newRow, newColumn);
        this.mine = false;
        this.flag = false;
        this.valueOfBox = 0;
    }

    // Return Box Number
    public byte getValueOfBox() { return valueOfBox; }

    // Mutable value Of Box to Increase Number/Value of Box Without Mine
    public void increaseValueOfBox() {
        valueOfBox++;
    }

    // Set Mine (Only true when Box is Mine)
    public void setMine() {
        mine = true;
    }

    // Check is Mine or Not
    public boolean isMine() { return mine; }

    // Moves & return integer Value
    public int moveUp() { return (int) COORDINATE.getX() - 1; }
    public int moveDown() { return (int) COORDINATE.getX() + 1; }
    public int moveLeft() { return (int) COORDINATE.getY() - 1; }
    public int moveRight() { return (int) COORDINATE.getY() + 1; }

    // TODO: Flags
    public void setFlag() {
        flag = true;
    }
    public void removeFlag() {
        flag = false;
    }
    public boolean isFlagged() {
        return flag;
    }

    public int getRow() {
        return (int) COORDINATE.getX();
    }
    public int getColumn() {
        return (int) COORDINATE.getY();
    }
}