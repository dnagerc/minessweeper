class MineChecker {
    // BOX AND BOARD OF BOX
    private final Box BOX;
    private final Box[][] BOARD_OF_BOXES;

    // POSSIBLE MINE POSITIONS
    private final int LEFT;
    private final int RIGHT;
    private final int UP;
    private final int DOWN;
    private final int ACTUAL_COLUMN;
    private final int ACTUAL_ROW;

    // Constructor
    public MineChecker(Box box, Box[][] BOARD_OF_BOXES) {
        this.BOX = box;
        this.BOARD_OF_BOXES = BOARD_OF_BOXES;

        this.ACTUAL_COLUMN = box.getColumn();
        this.ACTUAL_ROW = box.getRow();

        // MOVES (-1 -> Out Of Range)
        this.LEFT = Math.max(box.moveLeft(), -1);
        this.RIGHT = box.moveRight() < BOARD_OF_BOXES.length ? box.moveRight(): -1;
        this.UP = Math.max(box.moveUp(), -1);
        this.DOWN = box.moveDown() < BOARD_OF_BOXES[0].length ? box.moveDown() : -1;

    }

    // Check Mines Arround
    public void checkMines() {
        countUpperMines();
        countLowerMines();

        countLeftMines();
        countLeftDownMines();
        countLeftUpMines();

        countRightMines();
        countRightDownMines();
        countRightUpMines();
    }

    private void countRightUpMines() {
        if (RIGHT == -1 || UP == -1 || !BOARD_OF_BOXES[UP][RIGHT].isMine()) {
            return;
        }
        BOX.increaseValueOfBox();
    }

    private void countRightDownMines() {
        if (RIGHT == -1 || DOWN == -1 || !BOARD_OF_BOXES[DOWN][RIGHT].isMine()) {
            return;
        }
        BOX.increaseValueOfBox();
    }

    private void countRightMines() {
        if (RIGHT == -1 || !BOARD_OF_BOXES[ACTUAL_ROW][RIGHT].isMine()) {
            return;
        }
        BOX.increaseValueOfBox();
    }

    private void countUpperMines() {
        if (UP == -1 || !BOARD_OF_BOXES[UP][ACTUAL_COLUMN].isMine()) {
            return;
        }
        BOX.increaseValueOfBox();
    }

    private void countLowerMines() {
        if (DOWN == -1 || !BOARD_OF_BOXES[DOWN][ACTUAL_COLUMN].isMine()) {
            return;
        }
        BOX.increaseValueOfBox();
    }

    private void countLeftDownMines() {
        if (DOWN == -1 || LEFT == -1 || !BOARD_OF_BOXES[DOWN][LEFT].isMine()){
            return;
        }
        BOX.increaseValueOfBox();
    }

    private void countLeftMines() {
        if (LEFT == -1 || !BOARD_OF_BOXES[ACTUAL_ROW][LEFT].isMine()){
            return;
        }
        BOX.increaseValueOfBox();
    }

    private void countLeftUpMines() {
        if (LEFT == -1 || UP == -1 || !BOARD_OF_BOXES[UP][LEFT].isMine()){
            return;
        }
        BOX.increaseValueOfBox();
    }
}